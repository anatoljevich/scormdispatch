module Scormdispatch
  class Dispatch < Resource

# options 
# name - dispatch name 
# cid - courseId 
# did - destinationId 
# limit - limit
# Scormdispatch::Dispatch.create({:name => 'test', :cid => 11, :did => 11, :limit => 5})
# response: {"data"=>{"status"=>true, "message"=>"It\\s saved successfully", "id"=>11}}
    def self.create(options={})
      get("dispatch/createDispatch", options)
    end

    def self.update(options={})
      get("dispatch/updateDispatches", options)
    end
    
    # {"data"=>{"status"=>true, "dispatches"=>[
    #   {"id"=>11, "name"=>"Test -dispatch-coggno", "creator_id"=>18, "client_id"=>14, "course_id"=>11, "deliver"=>"mhFqdK-9rvWLj-SdtaZg-mAgZjH-vHFNRP", "access_limit"=>10, "blocked"=>0, "created_at"=>"2018-07-23 19:55:25", "updated_at"=>"2018-07-23 19:55:25", "courseTitle"=>"Sams-Test-Course", "destTitle"=>"Coggno.com"}, 
    #   {"id"=>12, "name"=>"Alex Dispatch", "creator_id"=>18, "client_id"=>14, "course_id"=>12, "deliver"=>"ejTap9-gPi1ry-vAYJJm-uHP2ek-aQTdc6", "access_limit"=>100, "blocked"=>0, "created_at"=>"2018-10-01 12:36:41", "updated_at"=>"2018-10-01 12:36:41", "courseTitle"=>"Test", "destTitle"=>"Coggno.com"}
    #   ]
    # }}
    def self.list(options={})
      get("dispatch/getDispatches", options)
    end

    # Scormdispatch::Dispatch.download({:dpid => 11, :learning => 'scorm_12'})
    def self.download(options={})
      get("dispatch/download", options)      
    end

    # Scormdispatch::Dispatch.get_history({:dpid => 11})
    # {"data"=>{"status"=>true, "histories"=>[{"id"=>11, "user"=>"Test, User", "course"=>11, "scorm"=>72, "invitation"=>0, "destination"=>14, "dispatch"=>6, "learner"=>"N,Sam", "complete_status"=>"incomplete", "satisfied_status"=>"failed", "score"=>"0.0", "total_time"=>"00:00:48.60", "attempt"=>"2", "created_at"=>"2018-07-23 19:57:39", "updated_at"=>"2018-07-23 19:58:53"}]}}
    def self.get_history(options={})
      get("dispatch/getHistories", options)
    end
        
    # Scormdispatch::Dispatch.delete({:dpid => 11})
    # {"data"=>{"status"=>true, "message"=>"It\\s deleted successfully"}}    
    def self.delete(options={})
      get("dispatch/deleteDispatches", options)
    end
    
  end
end
