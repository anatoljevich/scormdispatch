module Scormdispatch
  class Course < Resource

    def self.list(params={})
      get("dispatch/getDestinations", params)
    end

    # >> Scormdispatch::Course.create({}, {:title => 'title', :filedata => File.new("/tmp/file.zip", 'rb')})
    # => "{\"data\":{\"status\":111,\"data\":\"[ file.zip ] File Stored Successfully.\"}}<script>alert('The course has been uploaded successfully.');window.history.back();</script>"    

    def self.create(params={}, options={})
      post("course/importCourse", params, options)
    end

    def self.replace(params={}, options={})
      post("course/api_replace", params, options)
    end

    def self.update(params={}, options={})
      get("course/ApiSaveCourse", params, options)
    end

    def self.delete(params={}, options={})
      get("course/deleteCourse", params, options)
    end

  end
end

