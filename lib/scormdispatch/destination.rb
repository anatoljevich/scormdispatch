module Scormdispatch
  class Destination < Resource

# options 
# name - dispatch name 
# Scormdispatch::Destination.create({:name => 'test destination'})
# response: {"data"=>{"status"=>true, "id"=> 11}}
    def self.create(options={})
      get("dispatch/createDestination", options)
    end


    # {"data":{"status":true,"destinations":[{"id":11,"creator_id":18,"name":"Coggno.com","created_at":"2018-07-23 19:51:47","updated_at":"2018-07-23 19:55:00"},{"id":12,"creator_id":18,"name":"destination","created_at":"2018-10-09 17:18:47","updated_at":"2018-10-09 17:18:47"}]}}
    def self.list(options={})
      get("dispatch/getDestinations", options)
    end

  end
end
