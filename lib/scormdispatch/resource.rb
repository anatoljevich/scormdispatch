module Scormdispatch
  class Resource

    include Scormdispatch::Serializer

    def self.app_id
      Scormdispatch.app_id
    end
    
    def self.secret_key
      Scormdispatch.secret_key
    end

    def self.base_url
      Scormdispatch.base_url
    end

    def self.post(path, params={}, options={})
      url     = url_for(path)
      body    = encode(params)
      HTTP.post(url, body, options)
    end

    def self.put(path, params={})
      url     = url_for(path)
      body    = encode(params)
      HTTP.put(url, body)
    end

    def self.get(path, params={}, options={})
      url     = url_for(path, params)
      HTTP.get(url, options)
    end

    def self.delete(path, params={})
      url     = url_for(path, params)
      HTTP.delete(url)
    end


  protected
  
    def self.url_for(path, params = {})	
      sig =  Digest::MD5.hexdigest("#{secret_key}#{app_id}")
      url = "#{base_url}/#{path}/#{app_id}/#{sig}"
      if params.present?
        url += ('?' + params.collect{|key, value| "#{key}=#{CGI.escape(value.to_s)}"}.join('&'))
      end
      url
    end  

    def self.add_api_key_header(options)
      effective_api_key = options.delete(:api_key) || api_key

      if effective_api_key
        if options[:headers]
          options[:headers] = options[:headers].dup
        else
          options[:headers] = {}
        end
        options[:headers]["Zencoder-Api-Key"] = effective_api_key
      end

      options
    end

    def self.merge_params(options, params)
      if options[:params]
        options[:params] = options[:params].merge(params)
        options
      else
        options.merge(params: params)
      end
    end

  end
end
