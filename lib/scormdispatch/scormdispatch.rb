module Scormdispatch
        
    class << self
      attr_accessor :app_id, :secret_key, :base_url
      
    end

    self.app_id  = nil
    self.secret_key  = nil
    self.base_url = 'https://scormdispatch.co.uk/api/v1'

    def self.app_id
      @app_id || ENV['SCORMDISPATCH_APP_ID']
    end

    def self.secret_key
      @secret_key || ENV['SCORMDISPATCH_SECRET_KEY']
    end

    def self.base_url(env=nil)
      @base_url
    end

end
