Gem::Specification.new do |s|
  s.name = "scormdispatch"
  s.authors = ["Bobr Alexey"]
  s.email = "Alexi@coggno.com"
  s.description = "Scormdispatch.co.uk API ruby wrapper"
  s.version = "1.0.0"
  s.date = "2019-06-04"
  s.summary = "scormdispatch API ruby library"
  s.files = [
    "lib/scormdispatch.rb"
  ]
  s.require_paths = ["lib"]
end
